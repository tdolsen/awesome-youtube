import * as parser from "xml-parser"
import { readFileSync } from "fs";

const YOUTUBE_CHANNEL_BASE_URL = "https://www.youtube.com/channel/";

async function main() {
	const contents = await readFileSync("./dump.xml", { encoding: "utf-8" });
	const xml = parser(contents);
	const channels = xml.root.children[0].children[0].children;

	for (const channel of channels) {
		const { title, xmlUrl } = channel.attributes;
		const [,id] = xmlUrl.split("=");

		console.log(`* [${title}](${YOUTUBE_CHANNEL_BASE_URL}${id})`);
	}
}

void main();
